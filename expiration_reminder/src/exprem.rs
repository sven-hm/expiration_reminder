use crate::error::Error;
use ics::ICalendar;
use time::{Duration, OffsetDateTime};

#[derive(Debug, Clone)]
pub struct Notification {
    pub date: OffsetDateTime,
    pub message: String,
}

#[derive(Debug)]
pub struct ExpRem {
    pub id: String,
    pub expiration_date: OffsetDateTime,
    pub notifications: Vec<Notification>,
    pub subject: String,
    pub description: String,
}

#[derive(Debug)]
pub struct ExpRemCal<'t>(pub ICalendar<'t>);

impl ExpRem {
    pub fn add_relative_notification(
        &mut self,
        before: Duration,
        message: String,
    ) -> Result<(), Error> {
        self.notifications.push(Notification {
            date: self
                .expiration_date
                .checked_sub(before)
                .ok_or(Error::ExpRem(
                    "duration cannot be substracted from expiration date",
                ))?,
            message,
        });
        Ok(())
    }
}
