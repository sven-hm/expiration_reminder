use crate::error::Error;
use crate::exprem::{ExpRem, ExpRemCal};
use ics::properties::{Description, DtEnd, DtStart, Summary, Trigger};
use ics::{parameters, Alarm, Event, ICalendar};
use std::collections::HashSet;
use std::convert::TryFrom;
use time::format_description::well_known::{iso8601, Iso8601};
use time::OffsetDateTime;

const FORMAT_CONFIG: Iso8601<6646139979543554106528208308496498688> = Iso8601::<
    {
        iso8601::Config::DEFAULT
            .set_use_separators(false)
            .set_time_precision(iso8601::TimePrecision::Second {
                decimal_digits: None,
            })
            .encode()
    },
>;
static VERSION: &str = env!("CARGO_PKG_VERSION");

impl TryFrom<&[ExpRem]> for ExpRemCal<'_> {
    type Error = Error;

    fn try_from(exprems: &[ExpRem]) -> Result<Self, Self::Error> {
        let mut calendar = ICalendar::new("2.0", format!("expiration_reminder {VERSION}"));
        let now = OffsetDateTime::now_utc();
        let mut entries = HashSet::new();

        for exprem in exprems {
            if entries.insert(exprem.id.to_string()) {
                let mut event = Event::new(exprem.id.to_string(), now.format(&FORMAT_CONFIG)?);
                event.push(Summary::new(exprem.subject.clone()));
                let date = exprem.expiration_date.format(&FORMAT_CONFIG)?;
                event.push(DtStart::new(date.clone()));
                event.push(DtEnd::new(date));
                event.push(Description::new(exprem.description.replace('\n', "\\n")));

                if !exprem.notifications.is_empty() {
                    for notification in &exprem.notifications {
                        let mut trigger = Trigger::new(notification.date.format(&FORMAT_CONFIG)?);
                        trigger.append(parameters!("VALUE" => "DATE-TIME"));
                        event.add_alarm(Alarm::display(
                            trigger,
                            Description::new(notification.message.clone()),
                        ));
                    }
                }
                calendar.add_event(event);
            } else {
                // TODO log duplicate
            }
        }

        Ok(ExpRemCal(calendar))
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::exprem::Notification;
    use time::Duration;

    #[test]
    fn to_ics() {
        let now = OffsetDateTime::now_utc();

        let msg = "This should be a long
multiline string.

With lots of info.";

        let exprem = ExpRem {
            id: "12345".to_string(),
            expiration_date: now.checked_add(Duration::minutes(5)).unwrap(),
            notifications: vec![
                Notification {
                    date: now.checked_add(Duration::minutes(3)).unwrap(),
                    message: "first notification".to_string(),
                },
                Notification {
                    date: now.checked_add(Duration::minutes(4)).unwrap(),
                    message: "second notification".to_string(),
                },
            ],
            subject: "subject".to_string(),
            description: msg.to_string(),
        };

        let exprems = vec![exprem];
        let cal = ExpRemCal::try_from(exprems.as_slice()).unwrap();
        println!("--------------------------");
        println!("{}", cal.0.to_string());
        println!("--------------------------");
    }
}
