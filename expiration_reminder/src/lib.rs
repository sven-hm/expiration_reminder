mod error;
mod exprem;
mod from_x509_pem;
mod to_ics;

use crate::error::Error;
use crate::exprem::{ExpRem, ExpRemCal};
use time::Duration;
use x509_parser::pem::Pem;

pub fn ical_from_x509_chain(
    x509_cert: &str,
    notifications: Vec<(Duration, String)>,
) -> Result<String, Error> {
    let it = Pem::iter_from_buffer(x509_cert.as_bytes());
    let mut exprems: Vec<ExpRem> = vec![];
    for pem in it {
        let mut exprem = ExpRem::try_from(&pem?.parse_x509()?)?;
        for (before, message) in &notifications {
            exprem.add_relative_notification(*before, message.clone())?;
        }
        exprems.push(exprem);
    }

    let ecal = ExpRemCal::try_from(exprems.as_slice())?;

    Ok(ecal.0.to_string())
}

#[cfg(test)]
mod test {
    use super::*;
    use std::include_str;

    #[test]
    fn load_chain() {
        let cert_chain = include_str!("../test_data/cert_chain.pem");
        let ics = ical_from_x509_chain(
            &cert_chain,
            vec![
                (Duration::minutes(1), "1 minute before".to_string()),
                (Duration::hours(2), "2 hours before".to_string()),
                (Duration::days(3), "3 days before".to_string()),
            ],
        )
        .unwrap();
        println!("{ics}");
    }
}
