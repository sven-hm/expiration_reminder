use crate::error::Error;
use crate::exprem::ExpRem;
use std::convert::TryFrom;
use x509_parser::certificate::X509Certificate;

impl<'a> TryFrom<&X509Certificate<'a>> for ExpRem {
    type Error = Error;

    fn try_from(x509_cert: &X509Certificate<'a>) -> Result<Self, Self::Error> {
        Ok(ExpRem {
            id: x509_cert.serial.to_string(),
            expiration_date: x509_cert.validity.not_after.to_datetime(),
            notifications: vec![],
            subject: format!("Certificate expires: {}", x509_cert.subject),
            description: format!(
                "The following certificate expires:

serial: {} ({:x})
subject: {}
issuer: {}
note before: {}
note after: {}
",
                x509_cert.serial,
                x509_cert.serial,
                x509_cert.subject,
                x509_cert.issuer,
                x509_cert.validity.not_before,
                x509_cert.validity.not_after
            ),
        })
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use x509_parser::pem::Pem;

    #[test]
    fn load_from_pem() {
        let cert = include_str!("../test_data/cert.pem");
        for pem in Pem::iter_from_buffer(cert.as_bytes()) {
            let pem = pem.expect("Reading next PEM block failed");
            let x509 = pem.parse_x509().expect("X.509: decoding DER failed");
            let _exprem = ExpRem::try_from(&x509).unwrap();
        }
    }
}
