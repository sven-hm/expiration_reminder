#[derive(Debug)]
pub enum Error {
    X509(String),
    Ics(&'static str),
    ExpRem(&'static str),
    TimeFormat(time::error::Format),
}

impl std::fmt::Display for Error {
    fn fmt(&self, fmt: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Self::X509(err) => write!(fmt, "x509: {err}"),
            Self::Ics(err) => write!(fmt, "ics: {err}"),
            Self::ExpRem(err) => write!(fmt, "exprem: {err}"),
            Self::TimeFormat(err) => write!(fmt, "exprem: {err}"),
        }
    }
}

impl From<time::error::Format> for Error {
    fn from(err: time::error::Format) -> Self {
        Self::TimeFormat(err)
    }
}

impl From<x509_parser::nom::Err<x509_parser::error::X509Error>> for Error {
    fn from(err: x509_parser::nom::Err<x509_parser::error::X509Error>) -> Self {
        Self::X509(err.to_string())
    }
}

impl From<x509_parser::error::PEMError> for Error {
    fn from(err: x509_parser::error::PEMError) -> Self {
        Self::X509(err.to_string())
    }
}
