use clap::Parser;
use expiration_reminder::ical_from_x509_chain;
use std::{io, io::Write};
use time::Duration;

/// Generate reminder events from x509 certificate expiration dates.
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// Warning before expiration, e.g. `-w 1d` (1 day), `-w 1h` (1 hour)
    #[arg(short, long)]
    warning: Vec<String>,

    /// File name of ouput file (write to stdout by default)
    #[arg(short, long)]
    out: Option<String>,
}

fn main() -> Result<(), String> {
    let args = Args::parse();

    // Read data from stdin
    let pems = io::read_to_string(io::stdin()).map_err(|_| "Failed to read from stdin")?;

    // create notifications
    let mut notifications = vec![];
    for arg in &args.warning {
        let mut arg = arg.clone();
        notifications.push(match arg.pop() {
            Some(m) => match m {
                'm' => {
                    let before_minutes = arg
                        .parse::<i64>()
                        .map_err(|_| "Failed to parse minute value")?;
                    (
                        Duration::minutes(before_minutes),
                        format!("Certificate expires in {} minutes.", before_minutes),
                    )
                }
                'h' => {
                    let before_hours = arg
                        .parse::<i64>()
                        .map_err(|_| "Failed to parse hour value")?;
                    (
                        Duration::hours(before_hours),
                        format!("Certificate expires in {} hours.", before_hours),
                    )
                }
                'd' => {
                    let before_days = arg
                        .parse::<i64>()
                        .map_err(|_| "Failed to parse day value")?;
                    (
                        Duration::days(before_days),
                        format!("Certificate expires in {} days.", before_days),
                    )
                }
                _ => return Err("failed to parse parameters".to_string()),
            },
            None => return Err("failed to parse parameters".to_string()),
        });
    }

    let ics =
        ical_from_x509_chain(&pems, notifications).map_err(|err| format!("Failed: {}", err))?;

    match args.out {
        Some(out_file_name) => {
            let mut file = std::fs::File::create(out_file_name)
                .map_err(|_| "could not open file for writing")?;

            file.write(ics.as_bytes())
                .map_err(|_| "failed to write to file")?;
        }
        None => {
            println!("{}", ics);
        }
    }
    Ok(())
}
