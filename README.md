# expiration reminder

Simple tool to generate expiration reminder events in ics format from a list of PEM encoded x509 certificates.

## installation

Have the rust toolchains installed and this repo cloned, then do
```shell
cargo install --path expiration_reminder_cli/
```

## CLI usage examples

Generate a iCalendar file from a list of pem encoeded x509 certificates. Set notifications to 30 days, 3 days and 24 hours before each expiration.
```shell
cat cert1.pem cert2.pem cert3.pem | expiration_reminder -w 30d -w 3d -w 24h -o test.ics
```
Import `test.ics` into your calendar.
